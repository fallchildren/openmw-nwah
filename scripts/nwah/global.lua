local types = require("openmw.types")
local world = require("openmw.world")

local function onActorActive(actor)
   if actor.type ~= types.NPC then
      return
   end

   player = world.players[1]
   record = types.NPC.record(actor)

   if types.NPC.getBaseDisposition(actor, player) ~= 50 then
      print(string.format("%s already processed", record.name))
      return
   end

   p_is_dark_elf = types.NPC.record(player).race == "dark elf"

   mod = math.random(-10, 10)

   if record.race == "dark elf" then
      if math.random(0, 2) == 0 then
         mod = math.random(-10, 0)
      end

      if p_is_dark_elf then
         mod = mod - 11
      else
         mod = mod - 5
      end
   end

   if record.race == "argonian" and p_is_dark_elf then
      if mod > 0 and math.random(0, 4) == 0 then
         mod = math.random(-10, 0)
      end
   end

   if record.race == "khajiit" and p_is_dark_elf then
      if mod > 0 and math.random(0, 4) == 0 then
         mod = math.random(-10, 0)
      end
   end

   print(string.format("%s %d", record.name, mod))
   types.NPC.modifyBaseDisposition(actor, player, mod)
end

return {
    engineHandlers = {
       onActorActive = onActorActive
    }
}
